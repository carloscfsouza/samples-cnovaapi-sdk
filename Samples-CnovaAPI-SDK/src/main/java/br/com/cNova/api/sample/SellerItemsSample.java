package br.com.cNova.api.sample;

import java.util.List;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.sellerItems.SellerItem;
import br.com.cNova.api.resources.v2.sellerItems.SellerItems;
import br.com.cNova.api.resources.v2.sellerItems.SellerItemsResource;
import br.com.cNova.api.sample.utils.JSONPrinterUtil;
import br.com.cNova.api.sample.utils.Tokens;


public class SellerItemsSample {
	
	private SellerItemsResource api = new SellerItems(Hosts.SANDBOX,
			Tokens.client_id, Tokens.access_token);

	public void consultarItems(String offset, String limit, String site,
			String active, String levelIds, String title, String brand, String skuId,
			String skuSellerId, String quantity, String price, String crossDockingTime) throws ServiceException {
		
		System.out.println("Produtos associados:");
		
		List<SellerItem> sellerItems = api.getSellerItems(offset, limit, site,
				active, levelIds, title, brand, skuId, skuSellerId, quantity, price, crossDockingTime);
		
		System.out.println(sellerItems.size() + " - "
				+ JSONPrinterUtil.print(sellerItems));
	}

	public void getSellerItemBySkuSellerId(String skuSellerId, String offset, String limit, String site)
			throws ServiceException {
			System.out.println("Consultando SKU SELLER ID:" + skuSellerId);
			System.out.println(JSONPrinterUtil.print(api
					.getSellerItemBySkuSellerId(skuSellerId, offset, limit, site)));
	}

	public void atualizarPreco(String skuSellerId, Double defaultPrice,
			Double offer, String site) throws ServiceException {
			System.out.println("Atualizando pre�o para skuSellerId:" + skuSellerId);
			
			System.out.println(api.uptadePrice(skuSellerId, defaultPrice, offer, site));

	}

	public void atualizarEstoque(String skuSellerId, Integer quantity,
			Integer crossDockingTime, Integer warehouse)
			throws ServiceException {

		System.out.println("Atualizando estoque para skuSellerId:" + skuSellerId);
			System.out.println(api.uptadeStock(skuSellerId, quantity, crossDockingTime, warehouse));
	}
	
	public void atualizarStatus(String skuSellerId, boolean active, String site)
			throws ServiceException {

		System.out.println("Atualizando Status para skuSellerId:" + skuSellerId);
			System.out.println(api.uptadeStatus(skuSellerId, active, site));
	}
	
	public void consultarItemsStatusSelling(String offset, String limit, String site) throws ServiceException {
		
		System.out.println("Produtos associados:");
		
		List<SellerItem> sellerItems = api.getSellerItemsStatusSelling(offset, limit, site);
		
		System.out.println(sellerItems.size() + " - "
				+ JSONPrinterUtil.print(sellerItems));
	}

	public static void main(String[] args) {
		try {
			SellerItemsSample sellerItemsSample = new SellerItemsSample();
			
			String skuSellerId = "";
			String offset = "0";
			String limit = "10";
			String site = "";
			
			sellerItemsSample.consultarItemsStatusSelling(offset, limit, site);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}
}
