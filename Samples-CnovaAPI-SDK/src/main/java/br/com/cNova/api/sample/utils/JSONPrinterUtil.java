package br.com.cNova.api.sample.utils;

import org.codehaus.jackson.map.ObjectMapper;

public class JSONPrinterUtil {

	public static String print(Object obj) {

		String text = null;
		try {
			text = new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			text = "Error printing JSON: " + e.getMessage();
		}
		return text;
	}
}
