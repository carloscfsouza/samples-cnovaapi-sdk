package br.com.cNova.api.sample;

import java.io.File;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.loads.ProductLoad;
import br.com.cNova.api.pojo.v2.loads.ProductResponse;
import br.com.cNova.api.resources.v2.loads.Loads;
import br.com.cNova.api.resources.v2.loads.LoadsResource;
import br.com.cNova.api.sample.utils.JSONPrinterUtil;
import br.com.cNova.api.sample.utils.Tokens;

public class LoadsSample {

	private LoadsResource api = new Loads(Hosts.SANDBOX,
			Tokens.client_id2, Tokens.access_token2);

	public void carregarProdutos() throws ServiceException {
		ProductLoad products = new ProductLoad();
		File loads = new File("C:\\Users\\cfarias\\workspace\\Samples-CnovaAPI-SDK-v2\\Samples-CnovaAPI-SDK-v2\\Load_JSON_V2.json");

		products.setJsonFile(loads);
		System.out.println("Carga: "
				+ JSONPrinterUtil.print(api.loadProducts(products)));
	}

	public void consultarCargaForSkuSellerId(String... importerInfoId)
			throws ServiceException {
		for (String id : importerInfoId) {
			System.out.println("Carga: "
					+ JSONPrinterUtil.print(api.getLoadStatus(id)));

		}
	}

	public void consultarCarregarProdutos(String createdAt, String status,
			String offset, String limit)
					throws ServiceException {
		System.out.println("Carga: "
				+ JSONPrinterUtil.print(api.getLoadProducts(createdAt, status, offset, limit)));

	}

	public void alterarProduto(String skuSellerId) throws ServiceException {

		ProductResponse productResponse = api.getLoadStatus(skuSellerId);

		if (productResponse != null) {
			productResponse.getPrice().setOffer(Double.parseDouble("300"));

			System.out.println("Update produto: "
					+ JSONPrinterUtil.print(api.uptadeProducts(productResponse)));
		}
	}
	
	public void deleteProduto(String skuSellerId) throws ServiceException {

		ProductResponse productResponse = api.getLoadStatus(skuSellerId);

		if (productResponse != null) {

			System.out.println("Delete produto: "
					+ JSONPrinterUtil.print(api.deleteProducts(productResponse)));
		}
	}

	public static void main(String[] args) {
		LoadsSample test = new LoadsSample();
		try {
			//test.carregarProdutos();
			test.consultarCarregarProdutos("", "", "0", "50");
			//test.alterarProduto("1234");
			//
			
			test.consultarCargaForSkuSellerId("202355");
			
			
			//          test.aprovarCarga("202355");
			
			// test.consultarStatusProdutoCarga("103632", "122354");
		} catch (ServiceException e) {
			System.err.println(e);
		}
	}
}
