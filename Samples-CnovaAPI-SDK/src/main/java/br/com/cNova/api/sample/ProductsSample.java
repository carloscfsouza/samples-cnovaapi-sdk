package br.com.cNova.api.sample;

import java.util.ArrayList;
import java.util.List;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.products.Product;
import br.com.cNova.api.resources.v2.products.Products;
import br.com.cNova.api.resources.v2.products.ProductsResource;
import br.com.cNova.api.sample.utils.JSONPrinterUtil;
import br.com.cNova.api.sample.utils.Tokens;

public class ProductsSample {

	private ProductsResource api = new Products(Hosts.SANDBOX, Tokens.client_id,
			Tokens.access_token);

	public List<Product> consultarProduto(String productId)
			throws ServiceException {
		return api.getProduct(productId);
	}

	public static void main(String[] args) {
		try {
			ProductsSample productsSample = new ProductsSample();
			List<Product> produtos = new ArrayList<Product>();
			int count = 0;
			
			produtos = productsSample.consultarProduto("22842777");
			
			for (Product product : produtos) {

				System.out.println("#########################################");
				System.out.println("Produto: #" + ++count);
				System.out.println(JSONPrinterUtil.print(product));
				System.out.println("#########################################");
			}
			System.out.println(JSONPrinterUtil.print(productsSample.consultarProduto("22842777")));

		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}

}
