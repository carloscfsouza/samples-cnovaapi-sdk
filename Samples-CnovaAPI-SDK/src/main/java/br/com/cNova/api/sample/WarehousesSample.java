package br.com.cNova.api.sample;

import java.util.List;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.warehouses.Warehouse;
import br.com.cNova.api.resources.v2.warehouses.Warehouses;
import br.com.cNova.api.resources.v2.warehouses.WarehousesResource;
import br.com.cNova.api.sample.utils.Tokens;

public class WarehousesSample {

	private WarehousesResource api = new Warehouses(Hosts.SANDBOX, Tokens.client_id,
			Tokens.access_token);
	
	public List<br.com.cNova.api.pojo.v2.warehouses.Warehouse> getWarehouses()
			throws ServiceException {
		return api.getWarehouses();
	}
	
	public static void main(String[] args) {
		try {

			WarehousesSample warehousesSample = new WarehousesSample();
			
			List<br.com.cNova.api.pojo.v2.warehouses.Warehouse> list = warehousesSample.getWarehouses();
			
			if (list != null && list.size() > 0) {
				for (Warehouse warehouse : list) {
					System.out.println("================ WAREHOUSES ===================");
					System.out.println(warehouse.getId());
					System.out.println(warehouse.getSites());
					System.out.println("================ WAREHOUSES ===================");
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
